import React, {useState, useEffect, useContext} from 'react';
import {Form, Button,Row,Col} from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom'
import UserContext from '../UserContext';
import Swal from 'sweetalert2'

export default function Register(){
	
	const { user, setUser } = useContext(UserContext)
	const history = useNavigate();

	// State hooks to store the values of the input fields
	const [firstName,setFirstName]=useState('')
	const [lastName,setLastName]=useState('')
	const [email, setEmail] = useState('')
	const [mobile,setMobile]=useState('')
	const [password1, setPassword1] = useState('')
	const [password2, setPassword2] = useState('')
	const [addressLine1, setAddressLine1] = useState('')
	const [addressLine2, setAddressLine2] = useState('')
	const [city, setCity] = useState('')
	const [postal, setPostal] = useState('')
	const [country, setCountry] = useState('')
	const [isActive,setIsActive] = useState(false)


	function registerUser(e) {
        e.preventDefault()
        fetch('https://afternoon-plateau-51680.herokuapp.com/users/checkEmail', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({email: email})
        })
        .then(res => res.json())
        .then(data => {

        	console.log('HELLO', data)
            //alert(`====> data ${data}`)
            console.log(data)
            if(!data){
                //{
                fetch('https://afternoon-plateau-51680.herokuapp.com/users/register', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'Accept': 'application/json'
                    },
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,
                        email: email,
                        password: password1,
                        telephone: mobile,
                        address:{
                        	addressLine1: addressLine1,
                        	addressLine2: addressLine2,
                        	city: city,
                        	postal: postal,
                        	country: country

                        }
                    })
                })
                .then(res => res.json())
                .then(result => {
                	console.log('Kitty', result)
                    //alert(`====> data ${data}`)
                    if(!result){
                    	Swal.fire({
                            title: 'Something wrong',
                            icon: 'error',
                            text: 'Please try again.' 
                        });
                        clearfields()
                    } else{
                	    Swal.fire({
                	    title: `${firstName} ${lastName} is successfuly registered!`,
                	    icon: "success",
                	    text: "Enjoy shopping to Awesome Buys"
                		})
                		history("/login")
                    }
                    
                })               
                //}
            }else {
                Swal.fire({
                    title: "Duplicate Email Found!",
                    icon: "error",
                    text: "Please provide a different email."
            	})
            	clearfields()
            }
        })
    }

  	function clearfields(){
  		setFirstName('')
        setLastName('')
        setEmail('')
        setMobile('')
        setPassword1('')
        setPassword2('')
        setAddressLine1('')
        setAddressLine2('')
        setCity('')
        setPostal('')
        setCountry('')
  	}

	useEffect(()=>{
		if((email!== '' && password1!=='' && password2 !=='' && firstName!==''&& lastName!=='') && (password1===password2)){
			setIsActive(true)

		} else{
			setIsActive(false)
		}
	}, [email,password1,password2,firstName,lastName,mobile] )

	return (
		/*(user.id !== null) ?
		    <Navigate to="/login" />
		:*/
		<Form onSubmit={(e)=>registerUser(e)}>
			<h1 className="mt-5">Register</h1>
			<p className="mb-5">Already have an account? 
				<a href="/login">Sign in here.</a>
			</p>
			<Row className="mb-3">
				<Form.Group as={Col} className="mb-3" controlId="userFirstName">
					<Form.Label>First Name</Form.Label>
					<Form.Control 
					  	type="text" 
					  	placeholder="Enter first name" 
					  	value={firstName}
					  	onChange={e=>setFirstName(e.target.value)}
					  	required
					 />
				</Form.Group>
				<Form.Group as={Col} className="mb-3" controlId="userLastName">
					<Form.Label>Last Name</Form.Label>
					<Form.Control 
					  	type="text" 
					  	placeholder="Enter last name" 
					  	value={lastName}
					  	onChange={e=>setLastName(e.target.value)}
					  	required
					 />
				</Form.Group>
			</Row>
			
			<Row className="mb-3">
			  	<Form.Group as={Col} className="mb-3" controlId="userEmail">
			    <Form.Label>Email address</Form.Label>
			    <Form.Control 
			    	type="email" 
			    	placeholder="Enter email" 
			    	value={email}
			    	onChange={e=>setEmail(e.target.value)}
			    	required
			    />
			    
			  	</Form.Group>
			  	<Form.Group as={Col} className="mb-3" controlId="usermobile">
					<Form.Label>Mobile Number</Form.Label>
					<Form.Control 
					  	type="tel" 
					  	placeholder="0910-234-1111" 
					  	pattern="[0-9]{4}-[0-9]{3}-[0-9]{4}"
					  	value={mobile}
					  	onChange={e=>setMobile(e.target.value)}
					  	required
					 />
				</Form.Group>
			</Row>

			<Row className="mb-3">
				<Form.Group as={Col} className="mb-3" controlId="password1">
			    <Form.Label>Password</Form.Label>
			    <Form.Control 
				    type="password" 
				    placeholder="Password"
				    value={password1}
				    onChange={e=>setPassword1(e.target.value)}
				    required
			    />
			  	</Form.Group>
			    <Form.Group as={Col} className="mb-3" controlId="password2">
			      <Form.Label>Verify Password</Form.Label>
			      <Form.Control 
			  	    type="password" 
			  	    placeholder=" Verify Password"
			  	    value={password2}
				    onChange={e=>setPassword2(e.target.value)}
			  	    required
			      />
			    </Form.Group>
			</Row>
		  	
		  	
		    <Form.Group className="mb-3" controlId="addressLine1">
		          <Form.Label>Address</Form.Label>
		          <Form.Control 
		      	    type="text" 
		      	    placeholder="street Number/ room Number"
		      	    value={addressLine1}
		    	    onChange={e=>setAddressLine1(e.target.value)}
		      	    required
		        />
		    </Form.Group>
		    <Form.Group className="mb-3" controlId="addressLine2">
		          <Form.Label>Address 2</Form.Label>
		          <Form.Control 
		      	    type="text" 
		      	    placeholder="Village / Town "
		      	    value={addressLine2}
		    	    onChange={e=>setAddressLine2(e.target.value)}
		      	    required
		        />
		    </Form.Group>

		    <Row className="mb-3">
		    	<Form.Group as={Col} className="mb-3" controlId="city">
		    	      <Form.Label>City</Form.Label>
		    	      <Form.Control 
		    	  	    type="text" 
		    	  	    placeholder="City "
		    	  	    value={city}
		    		    onChange={e=>setCity(e.target.value)}
		    	  	    required
		    	    />
		    	</Form.Group>
		    	<Form.Group as={Col} className="mb-3" controlId="postal">
		    	      <Form.Label>Zip Code</Form.Label>
		    	      <Form.Control 
		    	  	    type="number" 
		    	  	    placeholder="postal code"
		    	  	    value={postal}
		    		    onChange={e=>setPostal(e.target.value)}
		    	  	    required
		    	    />
		    	</Form.Group>
		    	<Form.Group as={Col} className="mb-3" controlId="Country">
		    	      <Form.Label>Country</Form.Label>
		    	      <Form.Select 
		    	      	type="text"
		    	      	value={country}
		    	      	placeholder="select"
		    		    onChange={e=>setCountry(e.target.value)}
		    	  	    required>
		    	        <option>Philipines</option>
		    	        <option>Japan</option>
		    	        <option>Taiwan</option>
		    	        <option>Malaysia</option>
		    	        <option>China</option>
		    	      </Form.Select>
		    	</Form.Group>
		    </Row>

		    <Form.Group className="mb-3" id="formGridCheckbox">
		        <Form.Check type="checkbox" label="I accept the terms and conditions and I have read the privacy policy." />
		    </Form.Group>
		    


		    <Form.Group as={Row} className="m-4">
			
			{ 
				isActive ?
				<Button variant="primary" type="submit" id="submitBtn">
				  Submit
				</Button>
				:
				<Button variant="danger" type="submit" id="submitBtn" disabled>Submit
				</Button>
			}
			</Form.Group>
		  	
		</Form>
	)
}