import React, { useState, useEffect, useContext } from 'react';
import Nav from 'react-bootstrap/Nav';

function FillExample() {
  return (
    <Nav justify variant="tabs" className="mt-3 bg-warning " defaultActiveKey="/home">
      <Nav.Item>
        <Nav.Link href="/">Register Product</Nav.Link>
      </Nav.Item>
      <Nav.Item>
        <Nav.Link eventKey="link-1">Update Product</Nav.Link>
      </Nav.Item>
      <Nav.Item>
        <Nav.Link eventKey="link-2">Orders</Nav.Link>
      </Nav.Item>
      <Nav.Item>
        <Nav.Link eventKey="disabled" disabled>
          Disabled
        </Nav.Link>
      </Nav.Item>
    </Nav>
  );
}

export default FillExample;