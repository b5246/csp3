import React, { useContext, useEffect } from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
//import '../App.js';

export default function Logout(){

	const { unsetUser, setUser } = useContext(UserContext);
	

	unsetUser();
	localStorage.clear()
	sessionStorage.clear()

	useEffect(() => {
		setUser({id: null});
	})
	return(
		<Navigate to='/' />
	)
}