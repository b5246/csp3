import React, { Fragment } from 'react';
import Banner from '../components/Banner';
//import Welcome from '../components/Welcome';
import Highlights from '../components/Highlights';
import Featured from '../components/Featured';
// import CourseCard from '../components/CourseCard';
import {Col,Row,Button,Card,Form} from 'react-bootstrap'

export default function Home(){

	const data = {
	    title: "AWESOME DAY! Shoppers!",
	    content: "Big Sales and Greats Buys for everyone",
	    destination: "/products",
	    label: "SHOP NOW!"
	}

	return (
		<Row>

			<Featured/>
			<Banner data={data}/>

		</Row>
	)
}
