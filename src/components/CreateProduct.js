import React, {useState, useEffect, useContext} from 'react';
import {Form, Button,Row,Col} from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom'
import UserContext from '../UserContext';
import Swal from 'sweetalert2'

export default function CreateProduct(){
	
	const { user, setUser } = useContext(UserContext)
	const history = useNavigate();
	const [isActive,setIsActive] = useState(false)

	// State hooks to store the values of the input fields
	const [sku,setSku]=useState('')
	const [name,setName]=useState('')
	const [itemCategory, setItemCategory] = useState('')
	const [description,setDescription]=useState('')
	const [manufacturer, setManufacturer] = useState('')
	const [length, setLength] = useState('')
	const [heigth, setHeigth] = useState('')
	const [width, setWidth] = useState('')
	const [weight, setWeigth] = useState('')
	const [unitPrice, setUnitPrice] = useState('')
	const [stockQuantity, setStockQuantity] = useState('')
	const [image,setImage] = useState('')



	function registerProduct(e) {
        e.preventDefault()
        fetch('https://afternoon-plateau-51680.herokuapp.com/products', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`,
                'Accept': 'application/json'
            },
            body: JSON.stringify({
                SKU: sku,
                name: name,
                itemCategory: itemCategory,
                description: description,
                manufacturer: manufacturer,
                itemDetails:{
                	length: length,
                	heigth: heigth,
                	width: width,
                	weight: weight
                },
                unitPrice: unitPrice,
                stockQuantity: stockQuantity ,
                image: image
            })
        })
        .then(res => res.json())
        .then(data => {
        	console.log("HERE",data)
            //alert(`====> data ${data}`)
            if(!data){
            	Swal.fire({
                    title: 'Product exist!',
                    icon: 'error',
                    text: 'Check product details.' 
                });
                clearfields()
            } else{
        	    Swal.fire({
        	    title: `Product: ${name}`,
        	    icon: "success",
        	    text: "Successfuly Registered!"
        		})
        		history("/products")
            }
            
        })     
    }

  	function clearfields(){
  		setSku('')
  		setName('')
  		setItemCategory('')
  		setDescription('')
  		setManufacturer('')
  		setLength('')
  		setHeigth('')
  		setWeigth ('')
  		setWidth('')
  		setUnitPrice('')
  		setStockQuantity('')
  		setImage('')
  	}

	useEffect(()=>{
		if(sku!== '' && name!=='' && itemCategory !=='' && description!==''&& manufacturer!=='' && unitPrice!=='' && stockQuantity!=='' && image!==''){
			setIsActive(true)

		} else{
			setIsActive(false)
		}
	}, [sku,name,itemCategory,description,manufacturer,unitPrice,stockQuantity,image] )
	
	return (
		// ( user.id !== null) ?
		//     <Navigate to="/" />
		// :
		<Form onSubmit={(e)=>registerProduct(e)}>
			<h1 className="mt-0">Register Product</h1>
			<p className="mb-5">Please fill up the details below.
			</p>
			<Row className="mb-3">
				<Form.Group as={Col} className="mb-3" controlId="sku">
					<Form.Label>SKU</Form.Label>
					<Form.Control 
					  	type="text" 
					  	placeholder="Product Barcode" 
					  	value={sku}
					  	onChange={e=>setSku(e.target.value)}
					  	required
					 />
				</Form.Group>
				<Form.Group as={Col} className="mb-3" controlId="name">
					<Form.Label>Product Name</Form.Label>
					<Form.Control 
					  	type="text" 
					  	placeholder="Enter product name" 
					  	value={name}
					  	onChange={e=>setName(e.target.value)}
					  	required
					 />
				</Form.Group>
				<Form.Group as={Col} className="mb-3" controlId="itemCategory">
				      <Form.Label>Category</Form.Label>
				      <Form.Select 
				      	type="text"
				      	value={itemCategory}
				      	placeholder="select"
					    onChange={e=>setItemCategory(e.target.value)}
				  	    required>
				        <option>Beverage</option>
				        <option>Fruits</option>
				        <option>Snack</option>
				        <option>Frozen</option>
				        <option>Flavor</option>
				        <option>Bread</option>
				        <option>Condiments</option>
				      </Form.Select>
				</Form.Group>
			</Row>
			
			<Row className="mb-3">
			  	<Form.Group as={Col} className="mb-3" controlId="manufacturer">
					<Form.Label>Manufacturer</Form.Label>
					<Form.Control 
					  	type="text" 
					  	placeholder="Enter manufacturer" 
					  	value={manufacturer}
					  	onChange={e=>setManufacturer(e.target.value)}
					  	required
					 />
				</Form.Group>
			  	<Form.Group as={Col} className="mb-3" controlId="unitPrice">
			    <Form.Label>Price</Form.Label>
			    <Form.Control 
			    	type="number" 
			    	placeholder="Enter Price" 
			    	min ="0"
			    	value={unitPrice}
			    	onChange={e=>setUnitPrice(e.target.value)}
			    	required
			    />
			  	</Form.Group>
			  	<Form.Group as={Col} className="mb-3" controlId="stockQuantity">
					<Form.Label>Stocks</Form.Label>
					<Form.Control 
					  	type="number" 
					  	placeholder="Enter stock quantity" 
					  	min ="1"
					  	value={stockQuantity}
					  	onChange={e=>setStockQuantity(e.target.value)}
					  	required
					 />
				</Form.Group>
			</Row>
			<Row className="mb-3">
				<Form.Group as={Col} className="mb-3" controlId="length">
				      <Form.Label>Length</Form.Label>
				      <Form.Control 
				  	    type="number" 
				  	    placeholder="centimeter"
				  	    min ="1"
				  	    value={length}
					    onChange={e=>setLength(e.target.value)}
				  	    required
				    />
				</Form.Group>
				<Form.Group as={Col} className="mb-3" controlId="heigth">
				      <Form.Label>Height</Form.Label>
				      <Form.Control 
				  	    type="number" 
				  	    placeholder="centimeter"
				  	    min ="1"
				  	    value={heigth}
					    onChange={e=>setHeigth(e.target.value)}
				  	    required
				    />
				</Form.Group>
				<Form.Group as={Col} className="mb-3" controlId="width">
				      <Form.Label>Width</Form.Label>
				      <Form.Control 
				  	    type="number" 
				  	    placeholder="centimeter"
				  	    min ="1"
				  	    value={width}
					    onChange={e=>setWidth(e.target.value)}
				  	    required
				    />
				</Form.Group>
				<Form.Group as={Col} className="mb-3" controlId="weight">
				      <Form.Label>Weigth</Form.Label>
				      <Form.Control 
				  	    type="number" 
				  	    placeholder="centimeter"
				  	    value={weight}
					    onChange={e=>setWeigth(e.target.value)}
				  	    required
				    />
				</Form.Group>
			</Row>
			<Row className="mb-3">
				<Form.Group as={Col} className="mb-3" controlId="description">
			    <Form.Label>Description</Form.Label>
			    <Form.Control 
				    type="text" 
				    placeholder="Item Description/Isle No."
				    value={description}
				    onChange={e=>setDescription(e.target.value)}
				    required
			    />
			  	</Form.Group>
			    <Form.Group as={Col} className="mb-3" controlId="image">
			      <Form.Label>Product Image</Form.Label>
			      <Form.Control 
			  	    type="text" 
			  	    placeholder="Image link"
			  	    value={image}
				    onChange={e=>setImage(e.target.value)}
			  	    required
			      />
			    </Form.Group>
			</Row>

		    <Form.Group as={Row} className="m-4">
			
			{ 
				isActive ?
				<Button variant="primary" type="submit" id="submitBtn">
				  Submit
				</Button>
				:
				<Button variant="danger" type="submit" id="submitBtn" disabled>Submit
				</Button>
			}
			</Form.Group>
		  	
		</Form>
	)
}