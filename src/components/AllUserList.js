import React, { useState, useEffect,useContext } from 'react'
import PropTypes from 'prop-types';
import { Card, Row,Col,Button,ListGroup,Table, Container,Form } from 'react-bootstrap';
import {Link} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'
import { Navigate, useNavigate } from 'react-router-dom'
//import imagetemplate from '../img/grocery.jpg';


export default function AllUserList({userProp}) {
	

    const [usr,setUsr]= useState(false)
    const [usrId,setUsrId]=useState('')
    const history = useNavigate();


    function ordDetail(e){
		setUsr(true)
			
    }
    function close(){
    	setUsr(false)
    	let theFormItself =  document.getElementById('theForm')
    	theFormItself.style.display = 'none'
    }
	

	//console.log('PINK: ',userProp)
	function setUserAsAdmin(e){
    	fetch(`https://afternoon-plateau-51680.herokuapp.com/users/${userProp._id}/setAsAdmin`,{
				method: 'PUT',
				headers: {
				    Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			})
			.then(res => res.json())
			.then(data=>{
				//console.log('PINK:',data)
				if(data){
					    Swal.fire({
					    title: `${userProp.firstName} ${userProp.lastName} is now Admin`,
					    icon: "success",
					    text: "Check Credential"
						})
						//history("/")
						window.location.replace("/admin")
				} else{
						Swal.fire({
					        title: 'Something wrong',
					        icon: 'error',
					        text: 'Please try again.' 
					    });
					    //history("/admin")
					    window.location.replace("/admin")
				}
				
			})

		
		
	}

    
    return (

        

       <Container fluid >
           <Table  variant="light" hover >
                   <tbody>
                     <tr>
                       <Row>
                       <Col  ><td>{userProp._id}</td></Col>
                       
                       <Col  ><td>{userProp.firstName}</td></Col>
                       <Col  ><td>{userProp.lastName}</td></Col>
                       <Col  ><td>{String(userProp.isAdmin)}</td></Col>
                       
                 		{
                 			(usr===false)?

		                 		<Col className="justify-content-end">
		                 		<Row>
		                 			<Button  variant="outline-primary" className="btn mb-1 " onClick={ordDetail}>DETAILS</Button>
		                 			<Button  variant="outline-warning" className="btn mb-1 " onClick={setUserAsAdmin}>SET AS ADMIN</Button>
		                 		</Row>
                 		
                 		</Col>

		         		      		
                 		:
	                 		<Col className="justify-content-end">
	                 		<hr
	                 		        style={{
	                 		            color: 'gray'
	                 		        }}
	                 		    />
	                 		<Form id='theform' className="m-3 border" style={{width:'1000px'}} >
		         		      	      
		         		    	      <Row  className="m-1">
		         		    	      <Col>
		         		    	      		<Form.Label  column="sm" >
		         		    	      		  Email:
		         		    	      		</Form.Label>
		         		    	      	</Col>
		         		    	      	<Col>
		         		    	      		<Form.Label className=" text-primary" column="sm">
		         		    	      		  {userProp.email}
		         		    	      		</Form.Label>
		         		    	      	</Col>
		         		    	      	<Col>
		         		    	      		<Form.Label column="sm" >
		         		    	      		  Telephone: 
		         		    	      		</Form.Label>
		         		    	      	</Col>
		         		    	      	<Col>
		         		    	      		  <Form.Label  className=" text-primary" column="sm" >
		         		    	      		 {userProp.telephone}
		         		    	      		</Form.Label>
		         		    	      	</Col>
		         		    	      	<Col>
		         		    	      		<Form.Label  column="sm" >
		         		    	      		  Cart Total:
		         		    	      		</Form.Label>
		         		    	      	</Col>
		         		    	      	<Col>
		         		    	      		<Form.Label className=" text-primary" column="sm">
		         		    	      		  {userProp.cartTotal}
		         		    	      		</Form.Label>
		         		    	      	</Col>
     

		         			          </Row>
		         			          <Row  className="m-1">
		         			              <Form.Label column="sm" >
		         			                Address: 
		         			              </Form.Label>
		         			              
		         			                <Form.Label  className=" text-primary" column="sm" >
		         			               {userProp.address.addressLine1}
		         			              </Form.Label>
		         			              <Form.Label column="sm" >
		         			                Address 2: 
		         			              </Form.Label>
		         			              
		         			                <Form.Label  className=" text-primary" column="sm" >
		         			               {userProp.address.addressLine2}
		         			              </Form.Label>

		         			          </Row>
		         			          <Row  className="m-1">
		         			              <Form.Label column="sm" >
		         			               City: 
		         			              </Form.Label>
		         			              
		         			                <Form.Label  className=" text-primary" column="sm" >
		         			               {userProp.address.city}
		         			              </Form.Label>
		         			              
		         			              <Form.Label column="sm" >
		         			                Postal Code:
		         			              </Form.Label>
		         			              <Form.Label className=" text-primary" column="sm" >
		         			                {userProp.address.postal}
		         			              </Form.Label>
		         			              <Form.Label column="sm" >
		         			                Country:
		         			              </Form.Label>
		         			              <Form.Label className=" text-primary" column="sm" >
		         			                {userProp.address.country}
		         			              </Form.Label>

		         			          </Row>
		         			          
		         		      	    </Form>
                 		        
                 			<Row>
                 				<Button  variant="outline-danger" onClick={close} className="btn mb-1 " >Close</Button>
                 			</Row>	
	                 		
	                 		</Col>
                 		}
                 		

                       
                       </Row>

               
                     </tr>

           
                   </tbody>

                 </Table>
                 
            
       </Container>

       
    )
}

// ProductCard.propTypes = {
//     productProp: PropTypes.shape({
//         name: PropTypes.string.isRequired,
//         description: PropTypes.string.isRequired,
//         unitPrice: PropTypes.number.isRequired,
//         image: PropTypes.string.isRequired,
//         stockQuantity: PropTypes.number.isRequired
//     })
// }

