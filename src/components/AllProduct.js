import React, { Fragment, useEffect, useState, useContext } from 'react';
import UserContext from '../UserContext';
import ProductList from './ProductList'
import { Card, Row,Col,Button,ListGroup,Table,Form, Container,InputGroup } from 'react-bootstrap';
//import FloatingLabel from 'react-bootstrap/FloatingLabel';
//mport '../App.js';

export default function AllProducts() {
	const userIsAdmin = localStorage.getItem('isAdmin')

	 const { user ,setUser} = useContext(UserContext);

	const [products, setProducts] = useState([]);


	const fetchData = () => {
		
		fetch('https://afternoon-plateau-51680.herokuapp.com/products/all',{
			method: 'GET',
			headers: {
			    Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			//console.log('Product/ userIsAdmin: ', userIsAdmin);
			setProducts(data.map(product => {
			return (
				
					
					<ProductList key={product._id} productProp={product} />
				
				
			);
		}))
			
			
		})
	}
	useEffect(() => {

		fetchData()
		

	}, []);

	 
	return(

		<InputGroup className="m-1" >
        
      
		 <Table  >
                <thead >
                  <th>
                   <Row >
               
                    <Col  ><th >ID</th></Col>
                    <Col><th>SKU</th></Col>
                    <Col><th>Name</th></Col>
                    <Col><th>Stocks</th></Col>
                    <Col><th>Options</th></Col>
                    
                    </Row>

                  </th>

                </thead>
                
        <tbody>
        <tr><td>
        	<Row style={{width:'1000px'}} className='m-0'>{products}</Row></td></tr>
            
        </tbody>
      </Table>
		</InputGroup>


		
		
	)
}