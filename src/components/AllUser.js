import React, { Fragment, useEffect, useState, useContext } from 'react';
import UserContext from '../UserContext';
import AllUserList from './AllUserList'
import { Card, Row,Col,Button,ListGroup,Table,Form, Container,InputGroup } from 'react-bootstrap';
//import FloatingLabel from 'react-bootstrap/FloatingLabel';
//mport '../App.js';

export default function AllProducts() {
	const userIsAdmin = localStorage.getItem('isAdmin')

	 const { user ,setUser} = useContext(UserContext);

	const [cust, setCust] = useState([]);

	const fetchData = () => {

		
		
		fetch('https://afternoon-plateau-51680.herokuapp.com/users',{
			method: 'GET',
			headers: {
			    Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			//console.log('hello orders:',data.OrderDetails)
			setCust(data.map(customer => {
			return (
					<AllUserList key={customer._id} userProp={customer} />	
			);
			


		}))
			
			
		})
	}
	
	useEffect(() => {

		fetchData()
		

	}, []);

	 
	return(
		
      <InputGroup className="m-1 " >
		 <Table >
                <thead>
                  <th>
                   <Row >
                    <Col><th>ID</th></Col>

                    <Col><th>FirstName</th></Col>
                     <Col><th>LastName</th></Col>
                     <Col><th>isAdmin</th></Col>
                    <Col><th>Options</th></Col>
                    </Row>
                  </th>

                </thead>
                
        <tbody>
        	<Row>{cust}</Row>

            
        </tbody>
        
      </Table>
	</InputGroup>	

		
		
	)
}