import React, { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
//import image from '../img/tomato.jpg';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';

export default function UpdateProduct() {
	
	const { user } = useContext(UserContext)
	const history = useNavigate()
	const { productId } = useParams()

	const [name,setName]=useState('')
	const [itemCategory, setItemCategory] = useState('')
	const [description,setDescription]=useState('')
	const [manufacturer, setManufacturer] = useState('')
	const [unitPrice, setUnitPrice] = useState('')
	const [stockQuantity, setStockQuantity] = useState('')

	useEffect(()=> {
		fetch(`https://afternoon-plateau-51680.herokuapp.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			setName(data.name);
			setItemCategory(data.itemCategory)
			setDescription(data.description)
			setManufacturer(data.manufacturer)
			setUnitPrice(data.unitPrice)
			setStockQuantity(data.stockQuantity)
		})

	}, [productId])

	function productData(e){
		e.preventDefault()
		fetch(`https://afternoon-plateau-51680.herokuapp.com/products/${productId}`,{
			    method: 'PUT',
			    headers: {
			        'Content-Type': 'application/json',
			        Authorization: `Bearer ${localStorage.getItem('token')}`,
			        'Accept': 'application/json'
			    },
			    body: JSON.stringify({
			        name: name,
			        itemCategory: itemCategory,
			        description: description,
			        manufacturer: manufacturer,
			        unitPrice: unitPrice,
			        stockQuantity: stockQuantity
			    })
		})
		.then(res => res.json())
		.then(data => {
			console.log("HERE",data)
		    //alert(`====> data ${data}`)
		    if(!data){
		    	Swal.fire({
		            title: 'Something went wrong',
		            icon: 'error',
		            text: 'Check product details.' 
		        });
		       
		    } else{
			    Swal.fire({
			    title: `Product Updated`,
			    icon: "success",
			    text: "Check product details."
				})
				history("/admin")
		    }
		    
		})

		
	}





  return (
    <Form xs={1} md={2} class="mt-3" onSubmit={(e)=>productData(e)}>
    			<h1 className="mt-0">Update Product</h1>
    			<p className="mb-5">Please update up the details below.
    			</p>
    			<Row className="mb-3">
    				<Form.Group as={Col} className="mb-3" controlId="name">
    					<Form.Label>Product Name</Form.Label>
    					<Form.Control 
    					  	type="text" 
    					  	placeholder="Enter product name" 
    					  	value={name}
    					  	onChange={e=>setName(e.target.value)}
    					  	required
    					 />
    				</Form.Group>
    				<Form.Group as={Col} className="mb-3" controlId="itemCategory">
    				      <Form.Label>Category</Form.Label>
    				      <Form.Select 
    				      	type="text"
    				      	value={itemCategory}
    				      	placeholder="select"
    					    onChange={e=>setItemCategory(e.target.value)}
    				  	    required>
    				        <option>Beverage</option>
    				        <option>Fruits</option>
    				        <option>Snack</option>
    				        <option>Frozen</option>
    				        <option>Flavor</option>
    				        <option>Bread</option>
    				        <option>Condiments</option>
    				      </Form.Select>
    				</Form.Group>
    			</Row>
    			
    			<Row className="mb-3">
    			  	<Form.Group as={Col} className="mb-3" controlId="manufacturer">
    					<Form.Label>Manufacturer</Form.Label>
    					<Form.Control 
    					  	type="text" 
    					  	placeholder="Enter manufacturer" 
    					  	value={manufacturer}
    					  	onChange={e=>setManufacturer(e.target.value)}
    					  	required
    					 />
    				</Form.Group>
    			  	<Form.Group as={Col} className="mb-3" controlId="unitPrice">
    			    <Form.Label>Price</Form.Label>
    			    <Form.Control 
    			    	type="number" 
    			    	placeholder="Enter Price" 
    			    	min ="0"
    			    	value={unitPrice}
    			    	onChange={e=>setUnitPrice(e.target.value)}
    			    	required
    			    />
    			  	</Form.Group>
    			  	<Form.Group as={Col} className="mb-3" controlId="stockQuantity">
    					<Form.Label>Stocks</Form.Label>
    					<Form.Control 
    					  	type="number" 
    					  	placeholder="Enter stock quantity" 
    					  	min ="1"
    					  	value={stockQuantity}
    					  	onChange={e=>setStockQuantity(e.target.value)}
    					  	required
    					 />
    				</Form.Group>
    			</Row>
    			
    			<Row className="mb-3">
    				<Form.Group as={Col} className="mb-3" controlId="description">
    			    <Form.Label>Description</Form.Label>
    			    <Form.Control 
    				    type="text" 
    				    placeholder="Item Description/Isle No."
    				    value={description}
    				    onChange={e=>setDescription(e.target.value)}
    				    required
    			    />
    			  	</Form.Group>
    			</Row>

    		    <Form.Group  className="m-4">
    				<Row><Button variant="primary" type="submit" id="submitBtn">
    				 	UPDATE PRODUCT
    				</Button></Row>
    				
    			</Form.Group>
    				
    		  	
    		</Form>
  );
}
