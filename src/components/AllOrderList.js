import React, { useState, useEffect,useContext } from 'react'
import PropTypes from 'prop-types';
import { Card, Row,Col,Button,ListGroup,Table, Container,Form } from 'react-bootstrap';
import {Link} from 'react-router-dom'
import UserContext from '../UserContext'
//import imagetemplate from '../img/grocery.jpg';


export default function AllOrderList({orderProp}) {
    const [ord,setOrd]= useState(false)
    const [ordusr,setOrdusr] =useState('')
    const [prodId,setProdId] =useState('')


    function ordDetail(e){
    	//console.log('YELLOW',orderProp.userId)
    	
    	fetch(`https://afternoon-plateau-51680.herokuapp.com/products/${orderProp.OrderDetails.productId}`,{
				method: 'GET',
				headers: {
				    Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			})
			.then(res => res.json())
			.then(data=>{
				//console.log('CHECKING::::',data.name)
				setOrdusr(data.name)
				setProdId(data._id)
				
			})

		
		setOrd(true)
			//console.log('pppp',usrl.firstName)
    }
    function close(){
    	setOrd(false)
    	let theFormItself =  document.getElementById('theForm')
    	theFormItself.style.display = 'none'
    }
    return (

        

       <Container fluid >
           <Table  variant="light" hover >
                   <tbody>
                     <tr>
                       <Row>
                       <Col  ><td>{orderProp._id}</td></Col>
                       
                       <Col  ><td>Php {orderProp.totalAmount}</td></Col>

                       
                 		{
                 			(ord===false)?

		                 		<Col className="justify-content-end">
                 		<Button  variant="outline-primary" className="btn mb-1 " onClick={ordDetail}>DETAILS</Button>
                 		</Col>
		         		      		
                 		:
	                 		<Col className="justify-content-end">
	                 		<hr
	                 		        style={{
	                 		            color: 'gray'
	                 		        }}
	                 		    />
	                 		<Form id='theform' className="m-3 border" style={{width:'1000px'}} >
		         		      	      
		         		    	      <Row  className="m-1">
		         		    	      	<Col>
		         		    	      		<Form.Label column="sm" >
		         		    	      		  Product ID: 
		         		    	      		</Form.Label>
		         		    	      	</Col>
		         		    	      	<Col>
		         		    	      		  <Form.Label  className=" text-primary" column="sm" >
		         		    	      		 {prodId}
		         		    	      		</Form.Label>
		         		    	      	</Col>
		         		    	      	<Col>
		         		    	      		<Form.Label  column="sm" >
		         		    	      		  Product Name:
		         		    	      		</Form.Label>
		         		    	      	</Col>
		         		    	      	<Col>
		         		    	      		<Form.Label className=" text-primary" column="sm">
		         		    	      		  {ordusr}
		         		    	      		</Form.Label>
		         		    	      	</Col>
     

		         			          </Row>
		         			          <Row  className="m-1">
		         			              <Form.Label column="sm" >
		         			                Customer ID: 
		         			              </Form.Label>
		         			              
		         			                <Form.Label  className=" text-primary" column="sm" >
		         			               {orderProp.userId}
		         			              </Form.Label>
		         			              
		         			              <Form.Label column="sm" >
		         			                Transaction Date:
		         			              </Form.Label>
		         			              <Form.Label className=" text-primary" column="sm" >
		         			                {orderProp.purchasedAt}
		         			              </Form.Label>

		         			          </Row>
		         			          
		         		      	    </Form>
                 		        
                 			<Row>
                 				<Button  variant="outline-danger" onClick={close} className="btn mb-1 " >Close</Button>
                 			</Row>	
	                 		
	                 		</Col>
                 		}
                 		

                       
                       </Row>

               
                     </tr>

           
                   </tbody>

                 </Table>
                 
            
       </Container>

       
    )
}

// ProductCard.propTypes = {
//     productProp: PropTypes.shape({
//         name: PropTypes.string.isRequired,
//         description: PropTypes.string.isRequired,
//         unitPrice: PropTypes.number.isRequired,
//         image: PropTypes.string.isRequired,
//         stockQuantity: PropTypes.number.isRequired
//     })
// }

