import React, { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate ,useNavigate} from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
//import '../App.js';



export default function Login() {

        const { user, setUser } = useContext(UserContext);
        const navigate = useNavigate()
        const [email, setEmail] = useState('');
        const [password, setPassword] = useState('');
        const [isActive, setIsActive] = useState(false);
        const logId = localStorage.getItem('id')
        const [ord,setOrd]=useState()

        function authenticate(e) {
            e.preventDefault();
            fetch('https://afternoon-plateau-51680.herokuapp.com/users/login',{
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body:JSON.stringify({
                    email: email,
                    password: password
                })
            })
            .then(res=>res.json())
            .then(data=>{
                //console.log('USER TOKEN:',data)
                if (!data) {
                    Swal.fire({
                                title: "Email/Password is incorrect! ",
                                icon: "error",
                                text: "Check your login details and try again."
                            })
                    

                    
                    
                } else {
                    localStorage.setItem('token',data.access)

                    
                    fetch('https://afternoon-plateau-51680.herokuapp.com/users/details',{
                        method: 'GET',
                        headers: {
                            Authorization: `Bearer ${data.access}`
                        },
                        

                    })
                    .then(res=>res.json())
                    .then(result=>{
                        if(result){
                            Swal.fire({
                                title: "Login Successful",
                                icon: "success",
                                text: "Welcome to MY SHOP"
                            })
                            setOrd(result.purchaseHistory.reduce((sum,order)=>{
                                return sum + order.quantity
                            },0))
                            
                            localStorage.setItem('id',result._id)
                            localStorage.setItem('name',result.firstName)
                            localStorage.setItem('isAdmin',result.isAdmin)
                            //console.log('User details',result)

                            
                            
                            setUser({
                                id: result._id,
                                isAdmin: result.isAdmin
                            })
                            navigate('/')
                            
                            //console.log('USER TRAY:',user)
                            //console.log('USER isAdmin STORAGE:',isAdmin)
                        } else{
                            Swal.fire({
                                title: "Authentication Failed!",
                                icon: "error",
                                text: "Check your login details and try again."
                            })
                            
                        }
                        
                        
                    })

                }
            })
            setEmail('');
            setPassword('');
        }
        localStorage.setItem('ordctr',ord)
        //console.log('Filter:',ord)

        useEffect(() => {
            if(email !== '' && password !== ''){
                setIsActive(true);
            }else{
                setIsActive(false);
            }

        }, [email, password]);
        

        const formStyle = {
            //margin: '20px 0px 20px 300px',
            //padding: '10px',
            border: '1px solid #c9c9c9',
            borderRadius: '20px',
            background: 'light gray',
            width: '800px',
            display: 'block'
        };

        const labelStyle = {
            //margin: '10px 10px 5px 220px',
            //padding: '10px',
            fontFamily: 'Arial, Helvetica, sans-serif',
            fontSize: '15px'
};

        const submitStyle = {
            //margin: '20px 0 0 0',
            //padding: '7px 10px',
            border: '1px solid #efffff',
            borderRadius: '30px',
            //background: '#3085d6',
            width: '20%', 
            fontSize: '15px',
            color: 'white',
            //display: 'block'
        };

        

    return (

        

        <Form  style={labelStyle}  onSubmit={(e) => authenticate(e)}>

            <h1 className="mt-5">Login</h1>
            <p className="mb-5">Don't have a account yet? Create an 
                <a href="/register"> account here.</a>
            </p>
            <Form.Group as={Row}  className="mb-3"  controlId="userEmail">
                <Form.Label column lg={2} >Email address</Form.Label>
                <Col >
                    <Form.Control 
                        
                        
                        type="email" 
                        placeholder="Enter email" 
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        required
                    />
                </Col>
                
            </Form.Group>

            <Form.Group as={Row} className="mb-3"  controlId="password">
                <Form.Label column lg={2}>Password</Form.Label>
                <Col >
                <Form.Control 
                    
                    type="password" 
                    placeholder="Password" 
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                />
                </Col>
            </Form.Group>

            <Form.Group as={Row} className="m-4 g-2">
        <Col fluid sm={{ span: 10, offset: 5 }}>
            { isActive ? 
                <Button  variant="primary" type="submit" id="submitBtn">
                    Submit
                </Button>
                : 
                <Button  variant="danger" type="submit" id="submitBtn" disabled>
                    Submit
                </Button>
            }
            </Col>
      </Form.Group>
            

        </Form>

    )
}
