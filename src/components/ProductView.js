import React, { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
//import image from '../img/tomato.jpg';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';


export default function ProductView() {
	
	const usrAdmin = localStorage.getItem('isAdmin')

	//const orderctr = localStorage.getItem('ordctr')
	//const [ctr,setCtr]=useState(0)

	const { user } = useContext(UserContext)
	const history = useNavigate()
	const { productId } = useParams()

	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [unitPrice, setUnitPrice] = useState(0)
	const [stockQuantity, setStockQuantity]=useState(0)
	const [image, setImage]=useState("")
	const [orderQty,setOrderQty]=useState(0)
	const valQty = parseInt(orderQty)
	//console.log('typeof data', typeof usrAdmin)
	useEffect(()=> {
		fetch(`localhost:4000/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			setName(data.name);
			setDescription(data.description);
			setUnitPrice(data.unitPrice);
			setStockQuantity(data.stockQuantity)
			setImage(data.image)
		})

	}, [productId]);

	const addToCart = (e) => {
		if(usrAdmin==='true'){
			Swal.fire({
				title: " Admin Credential ",
				icon: "error",
				text: "Please create user account."
			})
			history("/products");
		} else{
			fetch('https://afternoon-plateau-51680.herokuapp.com/users/checkout', {
				method: "POST",
				headers: {
					"Content-Type": "application/json",
					Authorization: `Bearer ${localStorage.getItem('token')}`,	
					'Accept': 'application/json'
				},
				body: JSON.stringify({
					productId: productId,
					quantity: valQty
				})
			})
			.then(res => res.json())
			.then(data => {
				//console.log(typeof valQty)
				//console.log(data)
				if(data){


					//setCtr((parseInt(orderctr)+valQty))
					Swal.fire({
						title: "Added to Cart",
						icon: "success",
						text: "Item is placed to your cart"
					})
					history("/products");
				}
				else{
					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again."
					})
					history("/products");
				}
			})
		}
		
	}


	//localStorage.setItem('ordctr',ctr)

	return(
		<Row  xs={1} md={2} class="card  g-2">
			

			<Col className=" mt-4" class="card bg-light mt-3" >
		
				      
				      <Card.Body >
				        <Card.Title className="mb-4 font-weight-bolder text-primary border">{name}</Card.Title>
    				    <Card.Subtitle className="mb-2 text-danger">Description:</Card.Subtitle>
    				    <Card.Text>{description}</Card.Text>
    				    <Card.Subtitle className="mb-2 text-danger">Price:</Card.Subtitle>
    				    <Card.Text>Php {unitPrice}</Card.Text>
    				    <Card.Subtitle className="mb-2 text-danger">Stock :</Card.Subtitle>

    				    <Card.Text>Php {stockQuantity}</Card.Text>
    				    { user.id !== null ?
    				    	<InputGroup  className="mb-3">
    				    	    <Form.Control
    				    	    	type="number" 
    				    	    	min="0"
    				    	    	value={orderQty}
    				    	      	placeholder="Enter Quantity here"
    				    	      	onChange={e=>setOrderQty(e.target.value)}
    				    	      	required
    				    	    />
    				    	    <Button variant="primary" onClick={addToCart} id="submitBtn" block> + ADD to cart</Button>
    				    	 </InputGroup>
    				    	
    				    	:
    				    	<Link className="btn btn-danger btn-block"  to="/login">Log in to Place Order</Link>

    				    }

				      </Card.Body>
				      
				      </Col>
				    <Col class=" card bg-light mt-3 " >
				    
				    <Card.Body>
				    	<Card.Img src={image} />
				    </Card.Body>
				    </Col>
		</Row>

	)
}