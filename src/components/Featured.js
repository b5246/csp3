import React, { useState, useEffect, useContext } from 'react';
import Carousel from 'react-bootstrap/Carousel';
import image1 from '../img/featured1.jpg'
import image2 from '../img/featured5.jpg'
import image3 from '../img/featured2.jpg'

function UncontrolledExample() {
  return (
    <Carousel>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src={image3}
          alt="First slide"
          style={{ maxHeight: '500px' }}
        />
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src={image2}
          alt="Second slide"
          style={{ maxHeight: '500px' }}
        />


      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src={image1}
          alt="Third slide"
          style={{ maxHeight: '500px' }}
        />

      </Carousel.Item>
    </Carousel>
  );
}

export default UncontrolledExample;