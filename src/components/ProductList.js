import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types';
import { Card, Row,Col,Button,ListGroup,Table, Container,Form } from 'react-bootstrap';
import {Link} from 'react-router-dom'
import Swal from 'sweetalert2'
import { Navigate, useNavigate } from 'react-router-dom'
//import imagetemplate from '../img/grocery.jpg';


export default function ProductList({productProp}) {
    //const {_id, name, description, unitPrice, stockQuantity, image} = productProp;
    
        const [prod,setprod]= useState(false)

        const [usrId,setUsrId]=useState('')
        const history = useNavigate();


        function ordDetail(e){
            setprod(true)
                
        }
        function close(){
            setprod(false)
            let theFormItself =  document.getElementById('theForm')
            theFormItself.style.display = 'none'
        }
        
        function updateProduct(){
            history(`/admin/products/${productProp._id}`)
        }

        //console.log('PINK: ',userProp)
        function archiveProduct(e){
            fetch(`https://afternoon-plateau-51680.herokuapp.com/products/${productProp._id}/archive`,{
                    method: 'PUT',
                    headers: {
                        Authorization: `Bearer ${localStorage.getItem('token')}`
                    }
                })
                .then(res => res.json())
                .then(data=>{
                    //console.log('PINK:',data)
                    if(data){
                            Swal.fire({
                            title: `Product Archived!`,
                            icon: "success",
                            text: "Check Credential"
                            })
                            history("/")
                    } else{
                            Swal.fire({
                                title: 'Something wrong',
                                icon: 'error',
                                text: 'Please try again.' 
                            });
                            history("/admin")
                    }
                    
                })

            
            
        }

        
        return (

            

           <Container fluid >
               <Table  variant="light" hover >
                       <tbody>
                         <tr>
                           <Row>
                           <Col  ><td>{productProp._id}</td></Col>
                           <Col  ><td>{productProp.SKU}</td></Col>
                           <Col  ><td>{productProp.name}</td></Col>
                           <Col  ><td>{productProp.stockQuantity}</td></Col>
                           
                            {
                                (prod===false)?

                                    <Col className="justify-content-end">
                                    <Row>
                                        <Button  variant="outline-primary" className="btn mb-1 " onClick={ordDetail}>DETAILS</Button>
                                        <Button  variant="outline-danger" className="btn mb-1 " onClick={archiveProduct}>ARCHIVE</Button>
                                        <Button  variant="outline-warning" className="btn mb-1 " onClick={updateProduct}>UPDATE</Button>
                                    </Row>
                            
                            </Col>

                                        
                            :
                                <Col className="justify-content-end">
                                <hr
                                        style={{
                                            color: 'gray'
                                        }}
                                    />
                                <Form id='theform' className="m-3 border" style={{width:'900px'}} >
                                          
                                          <Row  className="m-1">

                                          <Col>
                                                <Form.Label  column="sm" >
                                                  isActive:
                                                </Form.Label>
                                            </Col>
                                            <Col>
                                                <Form.Label className=" text-primary" column="sm">
                                                  {String(productProp.isActive)}
                                                </Form.Label>
                                            </Col>
                                            <Col>
                                                <Form.Label  column="sm" >
                                                  Price:
                                                </Form.Label>
                                            </Col>
                                            <Col>
                                                <Form.Label className=" text-primary" column="sm">
                                                  Php {productProp.unitPrice}
                                                </Form.Label>
                                            </Col>
                                            <Col>
                                                <Form.Label column="sm" >
                                                  Category: 
                                                </Form.Label>
                                            </Col>
                                            <Col>
                                                  <Form.Label  className=" text-primary" column="sm" >
                                                 {productProp.itemCategory}
                                                </Form.Label>
                                            </Col>
                                          </Row>
                                          <Row  className="m-1">
                                              <Form.Label column="sm" >
                                                Manufacturer: 
                                              </Form.Label>
                                              
                                                <Form.Label  className=" text-primary" column="sm" >
                                               {productProp.manufacturer}
                                              </Form.Label>
                                              <Form.Label column="sm" >
                                                Description: 
                                              </Form.Label>
                                              
                                                <Form.Label  className=" text-primary" column="sm" >
                                               {productProp.description}
                                              </Form.Label>

                                          </Row>
                                          <Row  className="m-1">
                                              <Form.Label column="sm" >
                                               Length: 
                                              </Form.Label>
                                              
                                                <Form.Label  className=" text-primary" column="sm" >
                                               {productProp.itemDetails.length}
                                              </Form.Label>
                                              
                                              <Form.Label column="sm" >
                                                Height:
                                              </Form.Label>
                                              <Form.Label className=" text-primary" column="sm" >
                                                {productProp.itemDetails.heigth}
                                              </Form.Label>
                                              <Form.Label column="sm" >
                                                Width:
                                              </Form.Label>
                                              <Form.Label className=" text-primary" column="sm" >
                                                {productProp.itemDetails.width}
                                              </Form.Label>
                                              <Form.Label column="sm" >
                                                Weight:
                                              </Form.Label>
                                              <Form.Label className=" text-primary" column="sm" >
                                                {productProp.itemDetails.weigth}
                                              </Form.Label>

                                          </Row>
                                          
                                        </Form>
                                    
                                <Row>
                                    <Button  variant="outline-danger" onClick={close} className="btn mb-1 " >Close</Button>
                                </Row>  
                                
                                </Col>
                            }
                            

                           
                           </Row>

                   
                         </tr>

               
                       </tbody>

                     </Table>
                     
                
           </Container>

           
        )
}

// ProductCard.propTypes = {
//     productProp: PropTypes.shape({
//         name: PropTypes.string.isRequired,
//         description: PropTypes.string.isRequired,
//         unitPrice: PropTypes.number.isRequired,
//         image: PropTypes.string.isRequired,
//         stockQuantity: PropTypes.number.isRequired
//     })
// }

