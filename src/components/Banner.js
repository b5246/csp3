import React,{useState} from 'react'
import {Link, NavLink} from 'react-router-dom'
import {Col,Row,Button} from 'react-bootstrap'
import Badge from 'react-bootstrap/Badge';
import Placeholder from 'react-bootstrap/Placeholder';
import image from '../img/sale1.jpg'
import Image from "react-bootstrap/Image";

export default function Banner({data}){
	
	//console.log(data);
	const {title, content, destination, label} = data;
	return (
		<Row fluid="true" xs={1} md={2} class="card g-2">

			<Col class="card-body text-cente" >
				<Placeholder as="p" animation="glow">
			        <Placeholder xs={12} />
			    </Placeholder>
				<h2 class="text-danger text-center" >{title}</h2>
				<h5 class="text-center "><Badge bg="primary">{content}</Badge></h5>
				
				<Link to={destination} class="card text-center border-light" >{label}</Link>
				
			</Col>
			<Col class=" card-body  mb-1 " >
				<a href="/products">
				<Image src={image} fluid="true"/>
				</a>
				
			</Col>
  

		</Row>
		
	)
}

