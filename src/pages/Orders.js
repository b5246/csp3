import React, { Fragment, useEffect, useState, useContext } from 'react';
import OrderList from '../components/OrderList';
import Highlights from '../components/Highlights';
//import Prod from '../components/ProductCard';
import UserContext from '../UserContext';
import {Col,Row,Button,Card,Form,Table} from 'react-bootstrap'
import ListGroup from 'react-bootstrap/ListGroup';
import Tab from 'react-bootstrap/Tab';

export default function Orders() {
	const { user } = useContext(UserContext);
	const [userTotalAmount, setUserTotalAmount] = useState([])
	const [userhistory, setUserHistory] = useState([])
	//const [filter,setFilter]=useState()

	const fetchData = () => {
		
		fetch('https://afternoon-plateau-51680.herokuapp.com/users/myOrders',{
			    method: 'GET',
			    headers: {
			        'content-type': 'application/json',
			        Authorization: `Bearer ${localStorage.getItem('token')}`
			    },
			    })
		.then(res => res.json())
		.then(data => {
			//console.log('ORDER:',data);
			setUserTotalAmount(data.userTotalAmount)
			//setFilter(data.history.filter(result=>result.history.productId))
			//console.log('FILTER',filter)
			// setFilter(data.history.reduce((sum,order)=>{
			// 	return sum + order.quantity
			// },0))
			
			setUserHistory( data.history.map(order=>{
			//console.log('ORDER:',order);	
				return(
					<Row className="justify-content-md-center" xs={1} md={2} xs lg="1">
						<Col>
					<OrderList key={order._id} orderProp={order}/>
					</Col>
					</Row>
					)
				
			}))
		})
	}

	//localStorage.setItem('ordctr',filter)
	//console.log('FILTER',filter)
	useEffect(() => {
		fetchData()
	}, []);

	 
	return(
		<Fragment>
			<Tab.Container id="list-group-tabs-example" defaultActiveKey="#link1">

				<Card.Header className=" mb-3 text-center text-light border bg-danger border-warning  g-1 m-2"  xs={12}>
				<h3>My Cart</h3>
				<hr />
				<p>Total Amount: Php {userTotalAmount}</p>
				</Card.Header>

		      <Row>
		        <Col sm={4} >
		          <ListGroup >
		            <ListGroup.Item action href="#link1">
		              My Order
		            </ListGroup.Item>
		            <ListGroup.Item action href="#link2">
		              Payment
		            </ListGroup.Item>
		            <ListGroup.Item action href="/products">
		              Shop MORE!
		            </ListGroup.Item>
		            
		          </ListGroup>
		        </Col>
		        <Col sm={8}>
		          <Tab.Content>
		          	<Tab.Pane eventKey="#link1">
		          		<Row className="" xs={1} md={1} xs lg="2"class="fp-item-container g-1 m-3">
      	              {userhistory}</Row>
      	            </Tab.Pane>
      	            <Tab.Pane eventKey="#link2">
      	              
		          	</Tab.Pane>
		            <Tab.Pane eventKey="/products">
      	              
		          	</Tab.Pane>
		          </Tab.Content>
		        </Col>
		      </Row>
		    </Tab.Container>

		
				
		</Fragment>
		

		
		
	)
}