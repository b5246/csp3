import React, { Fragment, useEffect, useState, useContext } from 'react';
// import coursesData from '../data/coursesData';
import ProductCard from '../components/ProductCard';
import AdminNav from '../components/AdminNav';
import Highlights from '../components/Highlights';
import UserContext from '../UserContext';
//import './Product.css'
import {Col,Row,Button,Card,Form} from 'react-bootstrap'
//import FloatingLabel from 'react-bootstrap/FloatingLabel';
//mport '../App.js';

export default function Products() {
	const userIsAdmin = localStorage.getItem('isAdmin')

	 const { user ,setUser} = useContext(UserContext);

	const [products, setProducts] = useState([]);
	/*const [category, setCategory] = useState()
	const [filter, setFilter] = useState([])*/


	const fetchData = () => {
		
		fetch('https://afternoon-plateau-51680.herokuapp.com/products')
		.then(res => res.json())
		.then(data => {
			console.log('Product/ userIsAdmin: ', userIsAdmin);
				setProducts(data.map(product => {
				return (
					<Row className="justify-content-md-center" xs={1} md={2} xs lg="1">
						<Col>
						<ProductCard key={product._id} productProp={product} />
						</Col>
					</Row>
				);
			}))
			
			
		})
	}


	

	useEffect(() => {

		fetchData()
		

	}, []);

	 
	return(
		<Row>
		<Card.Header className=" mt-1 text-center text-light bg-danger   "  xs={12}>
			<h2>SHOP</h2>
			</Card.Header>
		
		<Highlights/>
		<Row  xs={1} md={1} xs lg="4"class="fp-item-container g-1 m-3">
				{products}	
			</Row>
		
		</Row>
		

		
		
	)
}