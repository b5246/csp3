import { Fragment, useState,useEffect } from 'react';
import { Container } from 'react-bootstrap';
import {BrowserRouter as Router} from 'react-router-dom'
import {Route, Routes} from 'react-router-dom'
import AppNavbar from './components/AppNavbar';
//import Banner from './components/Banner';
// import Highlights from './components/Highlights';
import Products from './pages/Products';
import ProductView from './components/ProductView'
import UpdateProduct from './components/UpdateProduct'
import Home from './pages/Home';
import Register from './pages/Register'
import Admin from './pages/Admin'
import LogIn from './pages/LogIn'
import NotFound from './pages/NotFound'
import Logout from './pages/Logout'
import Footer from './components/Footer';
import Order from './pages/Orders'
import './App.css';
import {UserProvider} from './UserContext'

function App() {

  const [user,setUser]= useState({
    id:localStorage.getItem('id'),
    isAdmin:localStorage.getItem('isAdmin'),
  })

  const unsetUser = () =>{
    localStorage.clear()
  }

  useEffect(()=>{
    console.log('USER IS ADMIN:',user.isAdmin)
    console.log(localStorage)
  },[user])

  return (
    <UserProvider value={{user,setUser,unsetUser}}>
      <Router>
        <AppNavbar/>
        <Container>
          <Routes>
            <Route path="/" element={<Home/>} />
            <Route path="/admin" element={<Admin/>} />
            <Route path="/products" element={<Products/>} />
            <Route path="/login" element={<LogIn/>} />
            <Route path="/register" element={<Register/>} />
            <Route path="/logout" element={<Logout/>} />
            <Route path="/products/:productId" element={<ProductView/>} />
            <Route path="/admin/products/:productId" element={<UpdateProduct/>} />
            <Route path="/order" element={<Order/>} />
            <Route path="*" element={<NotFound/>} />
          </Routes>
        </Container>
        <Footer/>
      </Router>
    </UserProvider> 
  );
}

export default App;
