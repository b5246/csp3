import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types';
import { Card, Button } from 'react-bootstrap';
import {Link} from 'react-router-dom'
import imagetemplate from '../img/grocery.jpg';


export default function ProductCard({productProp}) {
    const {_id, name, description, unitPrice, stockQuantity, image} = productProp;

    return (

        <Card  className="m-4">
            <Card.Img variant="top" src={image} />
            <Card.Body >
                <Card.Title className="text-center bg-light font-weight-bolder text-primary border">{name}</Card.Title>
                <Card.Subtitle className="mb-2 text-danger">Price: </Card.Subtitle>
                <Card.Text>  Php {unitPrice}</Card.Text>
                <Card.Subtitle className="mb-2 text-danger">Stock :    </Card.Subtitle>
                <Card.Text > {stockQuantity}</Card.Text>

                <Link  className="btn btn-primary" to={`/products/${_id}`}>Product Detail</Link>
            </Card.Body>
        </Card>
    )
}

ProductCard.propTypes = {
    productProp: PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        unitPrice: PropTypes.number.isRequired,
        image: PropTypes.string.isRequired,
        stockQuantity: PropTypes.number.isRequired
    })
}

