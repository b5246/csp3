import React, { Fragment, useEffect, useState, useContext } from 'react';
import UserContext from '../UserContext';
import AllOrderList from './AllOrderList'
import { Card, Row,Col,Button,ListGroup,Table,Form, Container,InputGroup } from 'react-bootstrap';
//import FloatingLabel from 'react-bootstrap/FloatingLabel';
//mport '../App.js';

export default function AllProducts() {
	const userIsAdmin = localStorage.getItem('isAdmin')

	 const { user ,setUser} = useContext(UserContext);

	const [orders, setOrders] = useState([]);
	let usr = localStorage.getItem('ordusername')
	const [prod, setProd] = useState([]);

	const fetchData = () => {

		
		
		fetch('https://afternoon-plateau-51680.herokuapp.com/users/orders',{
			method: 'GET',
			headers: {
			    Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			//console.log('hello orders:',data.OrderDetails)
			setOrders(data.map(order => {
			return (
					<AllOrderList key={order.productId} orderProp={order} />	
			);
			


		}))
			
			
		})
	}
	
	useEffect(() => {

		fetchData()
		

	}, []);

	 
	return(
		
      <InputGroup className="m-1 " >
		 <Table >
                <thead>
                  <th>
                   <Row >
                    <Col><th>ID</th></Col>

                    <Col><th>Total Amount</th></Col>
                    <Col><th>Options</th></Col>
                    </Row>
                  </th>

                </thead>
                
        <tbody>
        	<Row>{orders}</Row>

            
        </tbody>
        
      </Table>
	</InputGroup>	

		
		
	)
}