import React,{useState, useContext} from 'react'
// import Navbar from 'react-bootstrap/Navbar'
// import Nav from 'react-bootstrap/Nav'
import {Link, NavLink} from 'react-router-dom'
import UserContext from '../UserContext'
import Badge from 'react-bootstrap/Badge';
import Image from "react-bootstrap/Image";
import logo from '../img/logo.png'
import Stack from 'react-bootstrap/Stack';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import {Form} from 'react-bootstrap';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Offcanvas from 'react-bootstrap/Offcanvas'



function NavScrollExample() {


	const userName = localStorage.getItem('name')
	const userIsAdmin = localStorage.getItem('isAdmin')
	const ctr = localStorage.getItem('ordctr')
	const {user} = useContext(UserContext)
  return (
    <Navbar bg="warning" variant="light" expand="lg" sticky="top">
      <Container fluid>
        <Navbar.Brand as={Link} to="/" style={{ color: 'white'}, { width: '50px' },{ maxHeight: '150px' }}exact>
        	
        	<Image
        src={logo}
        rounded
      />
        	</Navbar.Brand>
        	
        <Navbar.Toggle aria-controls="navbarScroll" />
        <Navbar.Collapse id="navbarScroll">
          <Nav
            className="me-auto my-1 my-lg-0"
            navbarScroll>
            <Nav.Link as={Link} to="/" exact>HOME</Nav.Link>
           
            <Nav.Link as={Link} to="/products" exact>PRODUCTS</Nav.Link>
          
            {
	        	(user.id !== null) ?
	        		<Nav.Link as={Link} to="/logout" exact>LOGOUT</Nav.Link>
	        	:
	        	<React.Fragment>
	        		<Nav.Link as={Link} to="/login" exact>LOG IN</Nav.Link>
	        		
	        		<Nav.Link as={Link} to="/register" exact>SIGN UP</Nav.Link>
	        	</React.Fragment>
            }
            {/*<div class="vr"></div>*/}
            {
            	(userIsAdmin==='true')?

            	<Nav.Link as={Link} to="/admin" exact>DASHBOARD</Nav.Link>
            	:
            	<Nav.Link ></Nav.Link>

            }
          </Nav>



          <Nav variant="danger" >
          
          {
          	(user.id !== null) ?
          	<Link to="/order">
               <Button type="button"  variant="primary" fluid="true" className="me-2">
	      				
	      				<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-cart4" viewBox="0 0 16 16">
								  <path d="M0 2.5A.5.5 0 0 1 .5 2H2a.5.5 0 0 1 .485.379L2.89 4H14.5a.5.5 0 0 1 .485.621l-1.5 6A.5.5 0 0 1 13 11H4a.5.5 0 0 1-.485-.379L1.61 3H.5a.5.5 0 0 1-.5-.5zM3.14 5l.5 2H5V5H3.14zM6 5v2h2V5H6zm3 0v2h2V5H9zm3 0v2h1.36l.5-2H12zm1.11 3H12v2h.61l.5-2zM11 8H9v2h2V8zM8 8H6v2h2V8zM5 8H3.89l.5 2H5V8zm0 5a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm-2 1a2 2 0 1 1 4 0 2 2 0 0 1-4 0zm9-1a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm-2 1a2 2 0 1 1 4 0 2 2 0 0 1-4 0z"/>
								</svg> 
	      				 <Badge bg="danger">{ctr}</Badge>
	      				
	    					</Button>
           </Link>
           :
           <Link to="/login " disabled>
               <Button type="button"  variant="danger" fluid="true" className="me-2">
	      				
	      				<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-cart4" viewBox="0 0 16 16">
								  <path d="M0 2.5A.5.5 0 0 1 .5 2H2a.5.5 0 0 1 .485.379L2.89 4H14.5a.5.5 0 0 1 .485.621l-1.5 6A.5.5 0 0 1 13 11H4a.5.5 0 0 1-.485-.379L1.61 3H.5a.5.5 0 0 1-.5-.5zM3.14 5l.5 2H5V5H3.14zM6 5v2h2V5H6zm3 0v2h2V5H9zm3 0v2h1.36l.5-2H12zm1.11 3H12v2h.61l.5-2zM11 8H9v2h2V8zM8 8H6v2h2V8zM5 8H3.89l.5 2H5V8zm0 5a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm-2 1a2 2 0 1 1 4 0 2 2 0 0 1-4 0zm9-1a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm-2 1a2 2 0 1 1 4 0 2 2 0 0 1-4 0z"/>
								</svg> 
	      				 
	      				
	    					</Button>
           </Link>
          }
          
          
	    					
          	<Navbar.Collapse  className="justify-content-end">
          	          <Navbar.Text >
          	            Signed in as:  <a>{userName}</a>
          	          </Navbar.Text>
          	        </Navbar.Collapse>

          </Nav>
         

        </Navbar.Collapse>
        
      </Container>
    </Navbar>
  );
  
}

export default NavScrollExample;



