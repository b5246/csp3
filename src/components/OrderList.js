import React, { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col ,Table} from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
//import image from '../img/tomato.jpg';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';
import PropTypes from 'prop-types';



export default function OrderList({orderProp}) {
	const { user } = useContext(UserContext)
	//const history = useNavigate()
	//const { productId } = useParams()
	const {_id, productId, status,orderAt,quantity} = orderProp;

	const [productName, setProductName] = useState([])
	const [productPrice, setProductPrice] = useState([])
	const [productImage, setProductImage] = useState([])



	useEffect(()=> {
		fetch(`https://afternoon-plateau-51680.herokuapp.com/products/${productId}`,{
			method: "GET",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`,	
				'Accept': 'application/json'
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setProductName(data.name)
			setProductPrice(data.unitPrice)
			setProductImage(data.image)
			
		})

	}, []);

	// TODO : Update cart items

	return(
		
		
		          <Card style={{ width: '18rem' }} className="border g-2 border-warning m-2">
		            <Card.Img variant="top" src={productImage} />
		            <Card.Body>
		              <Card.Title>{productName}</Card.Title>
		              <Card.Text>Price:Php {productPrice}</Card.Text>
		              <Card.Text>Order Quantity:  {orderProp.quantity}</Card.Text>
		              
		            </Card.Body>
		            
		          </Card>
		        

		
	)
}

OrderList.propTypes = {
    orderProp: PropTypes.shape({
        productId:PropTypes.string.isRequired,
        quantity: PropTypes.number.isRequired,
        orderAt: PropTypes.string.isRequired,
        status: PropTypes.string.isRequired
    })
}

