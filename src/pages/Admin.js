import React, {Fragment, useState, useEffect, useContext} from 'react';
import {Form, Button, Card ,Row,Col} from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom'
import UserContext from '../UserContext';
import Swal from 'sweetalert2'
import ListGroup from 'react-bootstrap/ListGroup';
import Tab from 'react-bootstrap/Tab';
import CreateProduct from '../components/CreateProduct'
import AllProduct from '../components/AllProduct'
import AllOrder from '../components/AllOrders'
import AllUser from '../components/AllUser'
//import Register from './Register'


function Admin() {
	const usrName = localStorage.getItem('name')
  return (
  	<Fragment>
  		<Tab.Container id="list-group-tabs-example" defaultActiveKey="#aasas">
  			<Card.Header className=" m1-3 text-center text-warning border  border-warning font-weight-bolder g-1 m-2"  xs={12}>
			<h3>Dashboard</h3>
			</Card.Header>
  		  <Row>
  		    			
  		    <Col sm={3}>
  		      <ListGroup>
  		      	<Card.Header  className="text-dark border border-warning bg-warning" >
			<p >Welcome Admin:  {usrName}</p>
			</Card.Header>
              <ListGroup.Item action href="#3e3rrgrt">
                Register Product
              </ListGroup.Item>
  		        <ListGroup.Item action href="#aasas">
  		          Product
  		        </ListGroup.Item>
  		        <ListGroup.Item action href="#5465ju">
  		          Orders
  		        </ListGroup.Item>
  		        
  		        <ListGroup.Item action href="#98977">
  		          Users
  		        </ListGroup.Item>
  		      </ListGroup>
  		    </Col>
  		    <Col sm={9}>
  		      <Tab.Content>
  		      <Tab.Pane eventKey="#3e3rrgrt">
                < CreateProduct/>
              </Tab.Pane>
              <Tab.Pane eventKey="#aasas">
               <AllProduct/>
              </Tab.Pane>
              <Tab.Pane eventKey="#5465ju">
               <AllOrder/>
              </Tab.Pane>
              <Tab.Pane eventKey="#98977">
                <AllUser/>
              </Tab.Pane>
  		      </Tab.Content>
  		    </Col>
  		  </Row>
  		</Tab.Container>
  	</Fragment>
    
  );
}

export default Admin;